/*******************************************************************************
 *  Interactive Lua help from official manual
 *  Copyright: Michael Ambrus, 2023
 *
 *  Lua API iterator-function: icat()
 ******************************************************************************/
#include "local.h"

#include <assert.h>
#include <lua-help/help.h>
#include <stdlib.h>
#include <string.h>
#include <zip.h>

/***
List categories (closure)

@function c_cat
@treturn Name of section
@treturn Number of subjects
@treturn Size of section data
@treturn Offset index in archive-file
*/
static int c_cat(lua_State * L)
{
    struct sections *csec =
        (struct sections *)lua_topointer(L, lua_upvalueindex(1));
    if (!csec)
        return 0;

    struct sections *psec = csec->next;

    /* Update the closure data (Lua up-value) */
    if (psec)
        lua_pushlightuserdata(L, psec);
    else
        lua_pushnil(L);
    lua_pushvalue(L, -1);
    lua_replace(L, lua_upvalueindex(1));

    /* Return values */
    lua_pushstring(L, csec->section->name);
    lua_pushinteger(L, csec->section->entries);
    lua_pushinteger(L, csec->section->data_sz);
    lua_pushinteger(L, csec->section->sot);

    return 4;
}

/***
List categories (iterator)

@function l_cat
@treturn Corresponding closure function
*/
int l_cat(lua_State * L)
{
    struct sections *psec = sections->next;

    lua_pushlightuserdata(L, psec);
    lua_pushcclosure(L, &c_cat, 1);

    return 1;
}
