# Lua Help

## News

* 3 Aug, 2023

Hey, there's a [wiki!](https://gitlab.com/mambrus/lua-help/-/wikis/home)

## Description

Generated Lua help-text from official version specific reference manual
without file-system dependency. Text is indexed and stored in a zlib
C-array in .text section (RO). Emphasis is highly embedded targets or
security constrained VM's that either disable file system access or
don't have one.

## Installation

``` bash
luarocks install lua-help
```

Pre-requisites are tested during install and will fail. Expect a few
failures as project depends on tools and libraries to build (`luarocks`).
A sample of prerequisites that can fail.

### Executables (usually non version specific)

* xxd
* lynx
* awk
* ziptool

### Libraries

* libzip-dev

## Usage

``` bash
help=require("lua-help")
= help.version()
= help.help("dofile")
```

See `examples/`, `tests/` and `luabin/` for more inspiration.

## Support

* File a ticket
* e-mail

## Roadmap

* Get it build and run under more environments than `luarocks`.
* Docker-containers for supported versions (Lua 5.1 - 5.4)

## Contributing

As a Lua beginner I needed interactive help of Lua reference. The only
existing one is is hopelessly outdated and doesn't full-fill my
requirements.

Feel free to submit CR's and contact me if you'd like to discuss something.

## Authors and acknowledgment

* Michael Ambrus <michael AT ambrus.com>, Author [keybase link](https://keybase.io/mambrus)


## License

MIT

## Status

The project is very young. It's also the Authors very first
Lua-project :-)

## Releases

### `1.0.1-1`

Builds/installable over `luarocks`. Barely, i.e. if very lucky and happen to
have all the prerequisites.

### `1.0.2-1`

* `luarocks` build/install quirks from `1.0.1-1`
* Build-containers for CI/Test up and used verifying deployment before
  releasing.
* This README

Known issues:

* Regression on 5.2

### `1.0.3-1`

* Fix Lua 5.2 build issue
* Examples corrected

### `1.0.5-1`

* Make lua-help build installable system DSO's when built for
  [`xb-lua`](https://gitlab.com/mambrus/xb-lua)
