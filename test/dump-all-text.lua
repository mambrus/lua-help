#!/usr/bin/env lua
--[[
==================
dump-all-text.lua
==================

Author: Michael Ambrus
        July 2023

Iterate over all subjects and print it's text.
--]]

help = require("lua-help")

local silent, times = ...

if times == nil then times = 1 end
if silent == nil then silent = 0 end

silent = silent + 1
silent = silent - 1

local ts=os.time()
local i
local text
--silent = 0
for i=1, times do
    local num=0
    for cat, sub in help.imatch() do
        text = help.help(sub, cat);
        num = num +1;
        if silent ~= 1 then
            print("Helptext for", num, cat, sub)
            print(text)
        end
    end
end
print("Completed", times, "iterations in", os.time() - ts, "seconds")
