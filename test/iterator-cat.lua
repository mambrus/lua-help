#!/usr/bin/env lua
--[[
==================
iterator-cat.lua
==================

Author: Michael Ambrus
        July 2023

Test-program for the help<XY>.icat() iterator
I.e. the "list-category" iterator
--]]

help = require("lua-help")

print("name    subject size    offset")
print("======= ======= ======= ========")
for name, num, sz, ofs in help.icat() do
    print(name,num,sz,ofs)
end
