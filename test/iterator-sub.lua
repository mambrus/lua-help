#!/usr/bin/env lua
--[[
==================
iterator-sub.lua
==================

Author: Michael Ambrus
        July 2023

Test-program for the help<XY>.isub() iterator
I.e. the "list-subjects in category" iterator

--]]

help = require("lua-help")

for cat in help.icat() do
    for name, sz, ofs  in help.isub(cat) do
        print(cat , name, ofs, sz);
        loc = { help.locate(cat, name) }
        if  loc[1] ~= cat or
            loc[2] ~= name or
            loc[3] ~= ofs or
            loc[4] ~= sz
        then
            message = string.format(
                        "Iterator failure: [%s/%s-%d:%s] NEQ [%s/%s-%d:%s]",
                        cat, name, ofs, sz,
                        loc[1], loc[2], loc[3], loc[4]);
            error(message);
        end
    end
end
print("")
print("Test passed OK")
