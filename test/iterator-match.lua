#!/usr/bin/env lua
--[[
==================
iterator-match.lua
==================

Author: Michael Ambrus
        July 2023

Test-program for the help<XY>.imatch() iterator
I.e. the "list-matches" iterator

This is the C-version of the Lua equivalent under examples/ with the same name


--]]

help = require("lua-help")

local resub, recat, silent, times = ...

if resub == nil then resub = ".*" end
if recat == nil then recat = ".*" end
if times == nil then times = 1 end
if silent == nil then silent = 0 end

silent = silent + 1
silent = silent - 1

local ts=os.time()
local i
for i=1, times do
    local num=0
    for cat, sub in help.imatch( resub, recat ) do
        num = num +1;
        if silent ~= 1 then
            print(num, cat, sub);
        end
    end
end
print("Completed", times, "iterations in", os.time() - ts, "seconds")

