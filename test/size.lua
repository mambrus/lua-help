#!/usr/bin/env lua
--[[
==================
size.lua
==================

Author: Michael Ambrus
        July 2023

Test-program for the "size" function
--]]

help = require("lua-help")

szul, szu, szc = help.size();
print("Size in bytes of various compression stages")
print("===========================================")
print("Uncompressed including added indexes: ", szul);
print("Uncompressed original:                ", szu);
print("Compressed:                           ", szc);
print("");
print("Compression ratios");
print("===================");
cri=100.0*szc/szul;
crb=100.0*szc/szu;
print("% index:", cri);
print("% bare: ", crb);
