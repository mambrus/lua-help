/*******************************************************************************
 *  Interactive Lua help from official manual
 *  Copyright: Michael Ambrus, 2023
 *
 *  Simpler Lua API functions and VM registration-function
 ******************************************************************************/
#include "local.h"

#include <assert.h>
#include <lua.h>
#include <lua-extras.h>
#include <lua-help/help.h>
#include <stdlib.h>
#include <string.h>
#include <zip.h>

/***
Size of help-data file/C-struct in bytes

@function size
@treturn Size uncompressed including indexes
@treturn Size compressed "as is"
@treturn Size compressed
*/
static int l_size(lua_State * L)
{
    lua_pushinteger(L, help_ucfile_len);
    lua_pushinteger(L, help_ucdata_len);
    lua_pushinteger(L, help_zipdata_len);

    return 3;
}

/***
Output version information

@function version
@treturn String module version
@treturn String Git SHA1 for this module
@treturn String Help-text for nor Lua-version
*/
static int l_version(lua_State * L)
{
    lua_pushstring(L, VERSION);
    lua_pushstring(L, GIT_SHA1);
    lua_pushstring(L, FOR_LUA_VERSION);

    return 3;
}

/***
Thin wrapper around zip_locate

@function l_locate
@tparam Offset in archive-file
@tparam Size in bytes
@treturn Number of characters read & printed or negative on error.
*/
static int l_locate(lua_State * L)
{
    size_t len;
    const char *cat = luaL_checklstring(L, 1, &len);
    const char *sub = luaL_checklstring(L, 2, &len);
    struct znode znode = { 0 };
    if (zip_locate(cat, sub, &znode)) {
        lua_pushstring(L, znode.category);
        lua_pushstring(L, znode.name);
        lua_pushinteger(L, znode.offset);
        lua_pushinteger(L, znode.sz);
        return 4;
    }
    lua_pushnil(L);
    return 1;
}

/***
Print chunk of size from archive-file directly to stdout from offset

Function returns number of characters read and printed
Note: Functions is meant to be ultra light-weight on the VM

@function l_dump
@tparam Offset in archive-file
@tparam Size in bytes to read & dump
@treturn Number of characters read & printed or negative on error.
*/
static int l_dump(lua_State * L)
{
    unsigned dofs = luaL_checkinteger(L, 1);
    unsigned sz = luaL_checkinteger(L, 2);

    static const char *filename = INTERNAL_FILENAME;
    zip_file_t *zfp = NULL;
    zip_int64_t nbytes;
    char buf[sz + 1];
    memset(buf, 0, sz + 1);

    zfp = zip_fopen_offset(archive, filename, dofs, ZIP_FL_ENC_RAW);
    if (!zfp) {
        lua_pushinteger(L, -1);
        return 1;
    }
    nbytes = zip_fread(zfp, buf, sz);
    printf("%s\n", buf);
    fflush(stdout);

    lua_pushinteger(L, nbytes);
    return 1;
}

/***
Cut-out a chunk of data of size from archive-file from offset and push to
VM as a string.

Similar to l_dump but is not lightweight.

@function l_str
@tparam Offset in archive-file
@tparam Size in bytes to read & dump
@treturn Number of characters read & printed or negative on error.
*/
static int l_str(lua_State * L)
{
    unsigned dofs = luaL_checkinteger(L, 1);
    unsigned sz = luaL_checkinteger(L, 2);

    static const char *filename = INTERNAL_FILENAME;
    zip_file_t *zfp = NULL;
    zip_int64_t nbytes;
    char buf[sz + 1];
    memset(buf, 0, sz + 1);

    zfp = zip_fopen_offset(archive, filename, dofs, ZIP_FL_ENC_RAW);
    if (!zfp) {
        lua_pushinteger(L, -1);
        return 1;
    }
    nbytes = zip_fread(zfp, buf, sz);
    if (nbytes > 0) {
        lua_pushstring(L, buf);
        return 1;
    }

    lua_pushnil(L);
    return 1;
}

/***
Return help for subject in category in string

@function l_help
@tparam subject string
@tparam category string (optional)
@treturn Number of characters read & printed or negative on error.
*/
static int l_help(lua_State * L)
{
    size_t len;
    zip_int64_t nbytes;
    zip_file_t *zfp = NULL;
    const char *subject = luaL_checkstring(L, 1);
    const char *category = luaL_optlstring(L, 2, "*", &len);
    struct znode znode = { 0 };

    if (!zip_locate(category, subject, &znode)) {
        fprintf(stderr, "No help found for subject [%s] in category [%s]\n",
                subject, category);
    }

    static const char *filename = INTERNAL_FILENAME;
    char buf[znode.sz + 1];
    memset(buf, 0, znode.sz + 1);

    zfp = zip_fopen_offset(archive, filename, znode.offset, ZIP_FL_ENC_RAW);
    if (!zfp) {
        lua_pushinteger(L, -1);
        return 1;
    }
    nbytes = zip_fread(zfp, buf, znode.sz);
    if (nbytes > 0) {
        lua_pushstring(L, buf);
        return 1;
    }

    lua_pushnil(L);
    return 1;
}

int luaopen_help(lua_State * L)
{
    struct luaL_Reg FUNCTIONS[] = {
        {"size", l_size},
        {"version", l_version},
        {"icat", l_cat},
        {"isub", l_sub},
        {"imatch", l_rematch},
        {"locate", l_locate},
        {"dump", l_dump},
        {"str", l_str},
        {"help", l_help},
        {NULL, NULL}            /* sentinel */
    };

    if (init_zipper() != 0)
        return 0;

    luaL_newlib(L, FUNCTIONS);
    return 1;
}
