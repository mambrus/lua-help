/*******************************************************************************
 *  Interactive Lua help from official manual
 *  Copyright: Michael Ambrus, 2023
 *
 *  Essential management of zipped data-array, i.e. the generated C-struct
 *  (usually in file "help-allzip.c" in 'unsigned char help_zipdata[]')
 ******************************************************************************/
#include "local.h"

#include <zip.h>
#include <string.h>
#include <stdlib.h>

#undef NDEBUG
#include <assert.h>

zip_source_t *source = NULL;    /* Lib-zip's abstraction of the zip data in
                                   C-array */
zip_t *archive = NULL;          /* Lib-zips abstraction of the "archive" in
                                   the "source" */
unsigned char *help_zipdata = NULL; /* Reference to generated RO C-array as a RW
                                       array. I.e. utilize no need to initialize
                                       a huge data variale in RAM from ELF .text
                                       section */
struct sections *sections;      /* Head of linked list */

static struct sections *append_new(struct sections *last);
static int charsubs(char *str, char orig, char rep);
int read_lidx(zip_file_t * zfp, struct lidx *lidx);
zip_file_t *zip_fopen_offset(zip_t * archive,
                             const char *name,
                             unsigned offset, zip_flags_t flags);

/* Grow the list by creating a new sections struct and append it to "last",
 * Return the new sections or NULL on failure */
static struct sections *append_new(struct sections *last)
{
    struct section *section;
    struct sections *next;

    section = calloc(1, sizeof(struct section));
    if (!section)
        goto error;
    next = calloc(1, sizeof(struct sections));
    if (!next)
        goto error;

    next->section = section;
    next->prev = last;
    next->next = NULL;
    if (last)
        last->next = next;
    return next;

error:
    if (section)
        free(section);
    if (next)
        free(next);
    return NULL;
}

/* Substitute all occurrences of character "orig" with character "rep" in
 * string "str". Returns number or replacements */
static int charsubs(char *str, char orig, char rep)
{
    char *ix = str;
    int n = 0;
    while ((ix = strchr(ix, orig)) != NULL) {
        *ix++ = rep;
        n++;
    }
    return n;
}

/* Open filename in archive and forwards fp until offset by reading dummy
 * buffers.
 *
 * I.e. this function compensates for the lack of a seek-function operating
 * on compressed data */
zip_file_t *zip_fopen_offset(zip_t * archive,
                             const char *name,
                             unsigned offset, zip_flags_t flags)
{
    zip_file_t *zfp = NULL;
    char buf[BUF_SIZE];
    zip_int64_t nbytes;
    unsigned nfull = offset / BUF_SIZE;
    offset = offset - nfull * BUF_SIZE;

    zip_int64_t index = zip_name_locate(archive, name, flags);
    if (index < 0) {
        printf("No such file in archive: [%s]\n", name);
        return NULL;
    }

    zfp = zip_fopen_index(archive, index, 0);

    /* Read-drop multitude of full buffers */
    for (unsigned i = 0; i < nfull; i++) {
        nbytes = zip_fread(zfp, buf, BUF_SIZE);
        assert(nbytes == BUF_SIZE);
    }
    /* Read-drop remaining bytes */
    nbytes = zip_fread(zfp, buf, offset);
    assert(nbytes == offset);

    return zfp;
}

/* Read from current file-position an index line (lidx). Return bytes read
 * from file, 0 if EOF or -1 on error.
 *
 * Note: Upon successful read, zfp is closed and must be re-opened */
int read_lidx(zip_file_t * zfp, struct lidx *lidx)
{
    char buf[BUF_SIZE] = { 0 };
    int llen;
    int rc;
    zip_int64_t nbytes = zip_fread(zfp, buf, BUF_SIZE);

    if (nbytes < 1)
        return nbytes;
    rc = zip_fclose(zfp);
    assert(rc != -1);

    assert(lidx);
    *lidx = (struct lidx) { 0 };
    rc = sscanf(buf, "%d %s", &lidx->sz, lidx->name);
    if (rc == EOF)
        return -1;

    rc = charsubs(buf, '\n', 0);
    if (rc < 2)
        return -1;

    llen = strnlen(buf, BUF_SIZE);
    if (!llen)
        return -1;

    return llen + 1;
}

/* Append linked list of sections with a new index section */
static struct sections *new_index(zip_t * archive,
                                  const char *filename, struct sections *last)
{
    unsigned offset = last->section->soas;
    unsigned cnt;
    struct sections *psec = NULL;
    zip_file_t *zfp = NULL;
    struct lidx lidx = { 0 };
    int len;
    unsigned tsz, csz, rsz;     /* Total size, current size, referral size */

    psec = append_new(last);

    zfp = zip_fopen_offset(archive, filename, offset, ZIP_FL_ENC_RAW);
    assert(zfp);

    len = read_lidx(zfp, &lidx);
    assert(len >= 0);

    psec->section->content = INDEX;
    psec->section->name = strndup(lidx.name, SNAME_MAX);
    psec->section->sz = lidx.sz + len - 1;  /* All lidx headers + own section lidx */
    psec->section->soas = offset + psec->section->sz + 1;

    /* Iterate over entries to count them and their sums. Result is in "cnt"
     * and rsz */
    tsz = psec->section->sz - len + 1;  /* Subtract 1:st as it's already
                                           read */
    offset = offset + len;
    for (csz = 0, cnt = 0, rsz = 0; csz < tsz; offset += len, cnt++) {
        zfp = zip_fopen_offset(archive, filename, offset, ZIP_FL_ENC_RAW);
        assert(zfp);
        len = read_lidx(zfp, &lidx);
        rsz += lidx.sz;
        csz += len;
    }
    psec->section->entries = cnt;
    psec->section->data_sz = rsz;

    return psec;
}

/* Calculate sot, i.e. beginning of data of referral text  */
static struct sections *calc_sot(struct sections *curr, unsigned *offset)
{
    curr->section->sot = *offset;

    /* Calculate offset for next INDEX's "sot" by adding current ones data-size */
    *offset += curr->section->data_sz;
    return curr->next;
}

/* Locate where exactly in archive data belonging to a unique a key,
 * constituted by "category-name" + "subject-name", is located or return
 * error.
 *
 * If key is found, return where in archive it exists and it's size
 * in the result-parameter znode (struct znode*).
 *
 * A special category-name "*" can be used as wild-card to search in all
 * categories. First match will be returned if subject-name is not unique
 * amongst categories.
 *
 * USE WITH CARE even if first match for a certain
 * subject is what's intended, as order of categories is not guaranteed or
 * new categories can emerge.
 *
 * NOTE: Since final znodes for subjects are neither stored nor cached by
 * this function for memory consumption reasons, search in index-section is
 * done by brute-force in ascending order, as cumulative offsets to both
 * index and data-section is calculated simultaneously as part of the search
 * itself.
 *
 * This approach will not scale well with number of subjects. Even though
 * subjects in archive are stored sorted and a binary search would
 * potentially find a subject name quickly, the required
 * offset-calculations, especially for the index look-up which is used to
 * find the next/any subject-index, becomes a catch-22.
 *
 * As long as number or subjects are in the few hundreds and not multiple
 * thousands, this compromise between RO-storage, RAM-consumption and
 * temporal performance is deemed fitting for this project where one of it's
 * primary goals is to be embeddable in tiny micro-controllers that may lack
 * both file-system and be very RAM restricted.
 *
 * Returns:
 *  1 if found,  i.e. znode is updated
 *  0 not found, i.e. znode is untouched
 */
int zip_locate(const char *category, const char *name, struct znode *znode)
{
    struct sections *psec = sections->next;
    struct section *sect = NULL;
    unsigned dofs = 0;          /* Offset in data */
    unsigned iofs = 0;          /* Offset in index-section */
    zip_file_t *zfp = NULL;
    struct lidx lidx = { 0 };
    unsigned cnt;
    static const char *filename = INTERNAL_FILENAME;

    while (psec) {
        if ((strncmp(category, psec->section->name, SNAME_MAX) == 0) ||
            (strncmp(category, "*", SNAME_MAX) == 0)) {
            /* Directory/category found */
            sect = psec->section;
            dofs = sect->sot;   /* Set base offset for data */

            /* Calculate section offset */
            iofs = sect->soas - sect->sz - 1;

            /* Waste 1 read, the section header lidx */
            zfp = zip_fopen_offset(archive, filename, iofs, ZIP_FL_ENC_RAW);
            iofs += read_lidx(zfp, &lidx);
            assert(strncmp(lidx.name, sect->name, SNAME_MAX) == 0);

            for (cnt = 0; cnt < sect->entries; cnt++) {
                zfp = zip_fopen_offset(archive, filename, iofs, ZIP_FL_ENC_RAW);
                assert(zfp);
                iofs += read_lidx(zfp, &lidx);
                if (strncmp(lidx.name, name, SNAME_MAX) == 0) {
                    /* Found: Fill in znode and exit early */
                    znode->category = category;
                    znode->name = name;
                    znode->offset = dofs;   /* Current data-offset is where it is */
                    znode->sz = lidx.sz;
                    goto end;
                }
                /* Not found. If exists it must be beyond, therefor
                 * opportunistically accumulate offset so far */
                dofs += lidx.sz;
            }
            if (strncmp(category, "*", SNAME_MAX)) {
                psec = NULL;
                goto end;
            } else {
                psec = psec->next;
            }
        } else {
            psec = psec->next;
        }
    }

end:
    if (psec)
        return 1;
    return 0;
}

/* Module initializer */
int init_zipper(void)
{
    zip_error_t ec;
    char unsigned_buf[NDIGITS_USNIGNED + 2] = { 0 };
    struct sections *psec = NULL;
    zip_file_t *zfp = NULL;
    zip_int64_t nbytes;
    unsigned nidx = 0;          /* Helper for number of index sections */

    /* Module-global data initialization */
    help_zipdata = get_help_zipdata();
    source = zip_source_buffer_create(help_zipdata, help_zipdata_len, 0, &ec);
    archive = zip_open_from_source(source, ZIP_RDONLY, &ec);

    zfp = zip_fopen_offset(archive, INTERNAL_FILENAME, 0, ZIP_FL_ENC_RAW);
    nbytes = zip_fread(zfp, unsigned_buf, NDIGITS_USNIGNED);
    if (nbytes != NDIGITS_USNIGNED) {
        fprintf(stderr, "Internal format error detected\n");
        return 1;
    }
    if (sscanf(unsigned_buf, "%d", &nidx) != 1) {
        fprintf(stderr, "Internal format error detected: number of indexes\n");
        return 1;
    }
    zip_fclose(zfp);
    charsubs(unsigned_buf, '\n', 0);

    psec = append_new(psec);
    sections = psec;            /* Set the linked-list head */

    psec->section->content = HEADR;
    psec->section->name = INTERNAL_FILENAME;
    psec->section->sz = strnlen(unsigned_buf, NDIGITS_USNIGNED);
    psec->section->soas = psec->section->sz + 1;
    psec->section->entries = nidx;

    /* Read, create and append index sections */
    for (unsigned i = 0; i < nidx; i++) {
        psec = new_index(archive, INTERNAL_FILENAME, psec);
    }

    /* Calculate SOT ("start of text") for each data-section */

    /* The last INDEX section's "soas" is the start of all data, i.e. the
     * main/first data-offset*/
    unsigned offset = psec->section->soas;

    psec = sections->next;      /* No need to start from head as offsets are
                                   consecutive carry-over and the first one is
                                   known (see above) */

    for (unsigned i = 0; i < nidx; i++) {
        /* Offset is updated on each iteration hence a reference as it's
           value is a carry-over to the next */
        psec = calc_sot(psec, &offset);
    }

    /* Last offset should be the complete length of the uncompressed file */
    //fprintf(stderr, "Check equal--->:   %d %d\n", offset, help_ucfile_len);
    assert(offset == help_ucfile_len);

    return source == NULL ? 1 : 0;
}
