#ifndef lua_extras_h
#define lua_extras_h
/*******************************************************************************
 *  Extended Lua-API's in the same style
 *  Copyright: Michael Ambrus, 2023
 *
 ******************************************************************************/
#include <lua.h>

#if !defined LUA_VERSION_NUM || LUA_VERSION_NUM==501
#include <lauxlib.h>

void luaL_checkversion_(lua_State * L, lua_Number ver, size_t sz);
void luaL_setfuncs(lua_State * L, const luaL_Reg * l, int nup);
const lua_Number *lua_version(lua_State * L);

#define LUAL_NUMSIZES	(sizeof(lua_Integer)*16 + sizeof(lua_Number))
#define luaL_checkversion(L)  \
	  luaL_checkversion_(L, LUA_VERSION_NUM, LUAL_NUMSIZES)

#define luaL_newlibtable(L,l)	\
  lua_createtable(L, 0, sizeof(l)/sizeof((l)[0]) - 1)

#define luaL_newlib(L,l)  \
  (luaL_checkversion(L), luaL_newlibtable(L,l), luaL_setfuncs(L,l,0))

#endif

const char *lua_match(lua_State * L, const char *s, const char *pattern,
                      int init);
#endif                          //lua_extras_h
