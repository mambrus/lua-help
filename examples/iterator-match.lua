#!/usr/bin/env lua
--[[
==================
iterator-match.lua
==================

Author: Michael Ambrus
        July 2023

C-API imatch() iterator alternatives, but in Lua

Problem:
* From 2 inter-dependent STATEFUL iterators
* Create another iterator
* Without consuming memory
* I.e. it can't be allowed to pre-calculate the results

===============================================================================
Status: This solution is ACCEPTABLE
        Albeit BARELY, it gets the job done
===============================================================================
Hmm:    Solution requires two executives and synchronisation in-between
        which is a complicated way to solve a simple problem.
Reason: 1) A closure function can't use a stateful iterator directly as
           sub-iterators are constantly re-created (i.e. re-started)
        2) Not using coroutine and let outer-scope free-run and let closure
           return the result returns only the last one.

I.e. a new state-full iterator in C-API is probably motivated even including the
up-value hassle and C-to-Lua call-back for string.match to be using use the same
regexp flavor (or yet another lib)

The use of yield with arguments is pretty cool however and arises new ideas.
It's doubtful the terminology is technically correct: Yielding can't have
result's, only intermediate values (which are made accessible i.e. and is the
cool part). Pthread end-results are similar but requires a thread to exit (hence
loose it's state). Alternatively usage of queuing/IPC which is also a likewise
sledgehammer.

--]]

help = require("lua-help")
local resub, recat, silent, times = ...

if resub == nil then resub = ".*" end
if recat == nil then recat = ".*" end
if times == nil then times = 1 end
if silent == nil then silent = 0 end

silent = silent + 1
silent = silent - 1

function match_iter( resub, recat )
    local i = 0

    co = coroutine.create(function (resub, recat)
        for cat in help.icat() do
            if string.match(cat, recat) then
                for sub in help.isub(cat) do
                    if string.match(sub, resub) then
                        coroutine.yield(cat, sub)
                    end
                end
            end
        end
    end)

    return function ()
        i = i + 1
        local cs = {}
        if coroutine.status(co) ~= "dead" then
            cs = { coroutine.resume(co, resub, recat) }
            if cs[2] then
                return i, cs[2], cs[3]
            end
        else
            return nil
        end
    end
end

local ts=os.time()
local i
for i=1, times do
    for num, cat, sub in match_iter( resub, recat ) do
        if silent ~= 1 then
            print(num, cat, sub);
        end
    end
end
print("Completed", times, "iterations in", os.time() - ts, "seconds")
