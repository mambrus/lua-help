#!/usr/bin/env lua
--[[
==================
table-match.lua
==================

Author: Michael Ambrus
        July 2023

C-API imatch() iterator alternatives, but in Lua

Problem:
* From 2 inter-dependent STATEFUL iterators
* Create another iterator
* Without consuming memory
* I.e. it can't be allowed to pre-calculate the results
===============================================================================
Status: These solutions are FAILURE
===============================================================================
Motive: Memory as intermediate storage is needed. For big data-sets this
        approach would not work.

--]]

help = require("lua-help")
local arg={...}

function get_matches ( resub, recat )
    -- Build-up match-table
    local matches = {}

     for cat in help.icat() do
         if string.match(cat, recat) then
            -- Consider subjects in this category
             for sub in help.isub(cat) do
                 if string.match(sub, resub) then
                     -- match: Add to table
                     table.insert(matches, { cat, sub })
                 end
             end
         end
     end
     return matches
end

print("Matches from match-table")
print("========================")
m = get_matches( arg[1], arg[2] );
for n, t in pairs(m) do
  print( n, t[1], t[2] )
end

function match_iter( resub, recat )
    local i = 0

    local matches = {}
    matches = get_matches( resub, recat );

    return function ()
        i = i + 1
        if (matches[i] ~= nil) then
            return i, matches[i][1], matches[i][2];
        end
    end
end

print("Matches from table-iterator")
print("===========================")
for num, cat, sub in match_iter( arg[1], arg[2] ) do
     print(num, cat, sub);
end
