#!/usr/bin/env lua
--[[
==================
help.lua
==================

Author: Michael Ambrus
        July 2023

Use help(subject [,category))

--]]

help = require("lua-help")
local sub, cat = ...

if sub == nil then
    error("At least one argument is required")
end
if cat == nil then cat = "*" end

text = help.help(sub, cat);
if text == nil then
    print("Perhaps try any of the following:")
    if cat == "*" then
        loadfile("examples/iterator-match.lua")(sub)
    else
        loadfile("examples/iterator-match.lua")(sub,cat)
        print("")
        print("Or any of the following:")
        loadfile("examples/iterator-match.lua")(sub)
    end
else
    print(text)
end
