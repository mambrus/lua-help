/*******************************************************************************
 *  Interactive Lua help from official manual
 *  Copyright: Michael Ambrus, 2023
 *
 *  Lua API iterator-function: cat sub imatch( [subre [,catre]] )
 *
 *  For each regular expression match of both arguments, return "category"
 *  and "subject".
 *
 *  Both parameters are optional and default to RE wildcard (".*") if not
 *  given.
 ******************************************************************************/
#include "local.h"

#include <assert.h>
#include <lua-extras.h>
#include <lua-help/help.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <zip.h>

struct upvalues {
    struct sections *psections; /* Current index section */
    const char *subre;          /* Reg-exp subject */
    const char *catre;          /* Reg-exp category */
    unsigned iofs;              /* Current offset in index */
    unsigned cnt;               /* Current index count */
    char *cat;                  /* Current category name */
};

/* Helper-function to find_next(). Search and simultaneously advance
 * index-search states until match or end of index is reached, in which case
 * false is returned otherwise true. In either case relevant up-values are
 * updated.
 */
bool search_advance_index(lua_State * L,
                          struct upvalues *up,
                          const char *filename, struct lidx *lidx)
{
    struct section *psec = up->psections->section;
    bool isfound = false;
    zip_file_t *zfp;

    while (!isfound && up->cnt < psec->entries) {
        zfp = zip_fopen_offset(archive, filename, up->iofs, ZIP_FL_ENC_RAW);
        assert(zfp);
        up->iofs += read_lidx(zfp, lidx);
        up->cnt = up->cnt + 1;
        if (lua_match(L, lidx->name, up->subre, 1))
            isfound = true;
    }
    return isfound;
}

/* find_next() is the locomotive of this iterator.
 * Recursively search for matches in both categories and subjects.
 *
 * For each match update references "cat" and "sub" for iterator function to
 * return.
 *
 * During search, update up-values to maintain the state-fullness of the
 * iterator.
 */
static struct sections *find_next(lua_State * L,
                                  struct upvalues *up,
                                  const char **cat, char **sub)
{
    if (!up->psections) {
        /* Break recursion */
        return NULL;
    }

    struct lidx lidx = { 0 };
    static const char *filename = INTERNAL_FILENAME;
    zip_file_t *zfp;
    struct section *psec;

    psec = up->psections->section;

    if (up->cnt == 0) {
        /* Index start: Waste 1 read, the section header lidx */
        zfp = zip_fopen_offset(archive, filename, up->iofs, ZIP_FL_ENC_RAW);
        up->iofs += read_lidx(zfp, &lidx);
        assert(strncmp(lidx.name, psec->name, SNAME_MAX) == 0);
        if (!lua_match(L, psec->name, up->catre, 1)) {
            /* Section doesn't match */
            goto continue_next_index;
        } else {
            /* Section mach. Stick with it */
            up->cat = psec->name;   /* Store in closure-data */
        }
    }

    if (search_advance_index(L, up, filename, &lidx)) {
        /* Match found & references updated */
        *sub = strndup(lidx.name, SNAME_MAX);
        *cat = up->cat;
    } else {
        /* End of current index-section reached */
        goto continue_next_index;
    }
    return up->psections;

continue_next_index:
    /* Prepare for continuing in next index-section, then make recursive call */
    up->cnt = 0;
    up->iofs = psec->soas;
    up->psections = up->psections->next;
    return find_next(L, up, cat, sub);
}

/***
Re-match subjects (closure)

@function c_rematch
@treturn Category (string)
@treturn Subject (string)
*/
static int c_rematch(lua_State * L)
{
    /* Up-values */
    struct upvalues *up;

    /* Return values */
    const char *cat;
    char *sub;

    /* Get up-values */
    up = (struct upvalues *)lua_topointer(L, lua_upvalueindex(1));
    assert(up);

    up->psections = find_next(L, up, &cat, &sub);
    if (!up->psections) {
        /* Nothing more */
        free(up);
        return 0;
    }

    /* Push closure's return values */
    lua_pushstring(L, cat);
    lua_pushstring(L, sub);
    free(sub);
    return 2;
}

/***
List Re-match found subjects (iterator)

@function l_rematch
@tparam String: Subject regexp (optional)
@tparam String: Category regexp (optional)
@treturn Corresponding closure function
*/
int l_rematch(lua_State * L)
{
    size_t len;
    /* Up-values */
    struct upvalues *up = calloc(1, sizeof(struct upvalues));
    assert(up);

    up->subre = luaL_optlstring(L, 1, ".*", &len);
    up->catre = luaL_optlstring(L, 2, ".*", &len);
    up->cat = "Category unknown";

    if (!sections)
        goto ret_error;

    up->psections = sections->next; /* First INDEX-section is the first directly
                                       adjacent the HEADR-section from global
                                       list */
    if (!up->psections)
        goto ret_error;

    up->iofs = sections->section->soas; /* First index-sections offset */
    lua_pushlightuserdata(L, up);

    lua_pushcclosure(L, &c_rematch, 1);
    return 1;

ret_error:
    lua_pushstring(L, "Fatal error: Zip-archive index not found");
    lua_error(L);
    return 0;
}
