/*******************************************************************************
 *  Interactive Lua help from official manual
 *  Copyright: Michael Ambrus, 2023
 *
 *  Lua API iterator-function: isub()
 ******************************************************************************/
#include "local.h"

#include <assert.h>
#include <lua-help/help.h>
#include <stdlib.h>
#include <string.h>
#include <zip.h>

/***
List subjects (closure)

@function c_sub
@treturn Name of subject
@treturn Size of subject data
@treturn Offset index in archive-file
*/
static int c_sub(lua_State * L)
{
    /* Up-values */
    struct section *section = NULL;
    unsigned dofs = 0;          /* Offset in data */
    unsigned iofs = 0;          /* Offset in index-section */
    unsigned cnt = 0;           /* Subjects counted so far */

    struct lidx lidx = { 0 };
    static const char *filename = INTERNAL_FILENAME;
    zip_file_t *zfp = NULL;

    /* Get up-values */
    section = (struct section *)lua_topointer(L, lua_upvalueindex(1));
    if (!section) {
        /* Generate error (FIXME */
        return 0;
    }
    dofs = lua_tointeger(L, lua_upvalueindex(2));
    iofs = lua_tointeger(L, lua_upvalueindex(3));
    cnt = lua_tointeger(L, lua_upvalueindex(4));

    /* Advance iterator if there's more. Update up-values **for next iteration** */
    if (cnt < section->entries) {
        zfp = zip_fopen_offset(archive, filename, iofs, ZIP_FL_ENC_RAW);
        assert(zfp);
        iofs += read_lidx(zfp, &lidx);
        dofs += lidx.sz;
        cnt++;
    } else {
        /* End reached: Push nothing (nil) on return-stack nor update up-values */
        return 0;
    }

    /* Update changed up-values */
    lua_pushinteger(L, dofs);
    lua_pushvalue(L, -1);
    lua_replace(L, lua_upvalueindex(2));

    lua_pushinteger(L, iofs);
    lua_pushvalue(L, -1);
    lua_replace(L, lua_upvalueindex(3));

    lua_pushinteger(L, cnt);
    lua_pushvalue(L, -1);
    lua_replace(L, lua_upvalueindex(4));

    /* Push closure's return values */
    lua_pushstring(L, lidx.name);
    lua_pushinteger(L, lidx.sz);
    lua_pushinteger(L, dofs - lidx.sz); /* Iterator return current
                                           data-offset, not the next one */

    return 3;
}

/***
List subjects (iterator)

@function l_sub
@tparam Category (string)
@treturn Corresponding closure function
*/
int l_sub(lua_State * L)
{
    /* Up-values */
    struct section *section = NULL;
    unsigned dofs = 0;          /* Offset in data */
    unsigned iofs = 0;          /* Offset in index-section */
    unsigned cnt = 0;           /* Subjects counted so far */

    zip_file_t *zfp = NULL;
    struct sections *psec = sections->next;
    struct lidx lidx = { 0 };
    static const char *filename = INTERNAL_FILENAME;
    size_t len;
    const char *category = luaL_checklstring(L, 1, &len);

    while (psec) {
        section = psec->section;
        if (strncmp(category, section->name, SNAME_MAX) == 0) {
            /* Directory/category found */
            dofs = section->sot;    /* Set base offset for data */

            /* Calculate section offset */
            iofs = section->soas - psec->section->sz - 1;
            assert(iofs == psec->prev->section->soas);

            /* Waste 1 read, the section header lidx */
            zfp = zip_fopen_offset(archive, filename, iofs, ZIP_FL_ENC_RAW);
            iofs += read_lidx(zfp, &lidx);
            assert(strncmp(lidx.name, section->name, SNAME_MAX) == 0);
            goto end;
        } else {
            psec = psec->next;
        }
    }

end:
    if (psec) {
        lua_pushlightuserdata(L, section);
        lua_pushinteger(L, dofs);
        lua_pushinteger(L, iofs);
        lua_pushinteger(L, cnt);
        lua_pushcclosure(L, &c_sub, 4);
        return 1;
    }
    /* Category not found: Generate error (FIXME) */
    return 0;
}
