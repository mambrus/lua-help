/*******************************************************************************
 *  Extended Lua-API's in the same style
 *  Copyright: Michael Ambrus, 2023
 *
 ******************************************************************************/
#include <lua-extras.h>

#undef UNUSED
#define UNUSED(X)  ((void)(X))

/***
Wrapper of the corresponding Lua-function, i.e.:
string.match (s, pattern [, init])

@function lua_match
@tparam Pointer to Lua VM (a.k.a. "state")
@tparam String to search in
@tparam Pattern to use as match
@tparam Start index starting from 1. Negative value accepted.
@treturn Matching string or NULL if no match
*/
const char *lua_match(lua_State * L, const char *s, const char *pattern,
                      int init)
{
    const char *result;
    int rc;
    UNUSED(rc);

    /* Find and push the "string" table on top of lua stack */
    int stringtable;
#if LUA_VERSION_NUM > 502
    rc = lua_getglobal(L, "string");
    if (rc == 0) {
        lua_pushstring(L,
                       "Fatal error: Table \"string\""
                       " doesn't exist in global table");
        lua_error(L);
    }
#else
    lua_getglobal(L, "string");
#endif
    stringtable = lua_gettop(L);

    /* Push the "match" function on the top of the lua stack */
#if LUA_VERSION_NUM > 502
    rc = lua_getfield(L, stringtable, "match");
    if (rc == 0) {
        lua_pushstring(L,
                       "Fatal error: Function \"match\""
                       " doesn't exist in \"string\" table");
        lua_error(L);
    }
#else
    lua_getfield(L, stringtable, "match");
#endif

    /* Push arguments on the top of the lua stack */
    lua_pushstring(L, s);
    lua_pushstring(L, pattern);
    lua_pushinteger(L, init);
    /* Call the function with 3 arguments, returning 1 result */
    if (lua_pcall(L, 3, 1, 0) != 0) {
        lua_error(L);
    }
    /* Get the result */
    result = lua_tostring(L, -1);
    /* Pop (remove) the function */
    lua_pop(L, 1);
    return result;
}

#if !defined LUA_VERSION_NUM || LUA_VERSION_NUM==501
#include <lauxlib.h>
/*
** Adapted from Lua 5.2.0
*/
void luaL_setfuncs(lua_State * L, const luaL_Reg * l, int nup)
{
    luaL_checkstack(L, nup + 1, "too many upvalues");
    for (; l->name != NULL; l++) {  /* fill the table with given functions */
        int i;
        lua_pushstring(L, l->name);
        for (i = 0; i < nup; i++)   /* copy upvalues to the top */
            lua_pushvalue(L, -(nup + 1));
        lua_pushcclosure(L, l->func, nup);  /* closure with those upvalues */
        lua_settable(L, -(nup + 3));
    }
    lua_pop(L, nup);            /* remove upvalues */
}

const lua_Number *lua_version(lua_State * L)
{
    static const lua_Number version = LUA_VERSION_NUM;
    if (L == NULL)
        return &version;
    else
        return &version;
}

void luaL_checkversion_(lua_State * L, lua_Number ver, size_t sz)
{
    const lua_Number *v = lua_version(L);
    if (sz != LUAL_NUMSIZES)    /* check numeric types */
        luaL_error(L, "core and library have incompatible numeric types");
    if (v != lua_version(NULL))
        luaL_error(L, "multiple Lua VMs detected");
    else if (*v != ver)
        luaL_error(L, "version mismatch: app. needs %f, Lua core provides %f",
                   ver, *v);
}
#endif
