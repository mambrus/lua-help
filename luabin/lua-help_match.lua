#!/usr/bin/env lua
--[[
==================
lua-help_match.lua
==================

Author: Michael Ambrus
        July 2023

Search for regular expression matches of subjects in categories 

@function lua-help_match
@tparam String: Subject regexp (optional)
@tparam String: Category regexp (optional)
@treturn List of matches
--]]

help = require("lua-help")

local resub, recat, silent, times = ...

if resub == nil then resub = ".*" end
if recat == nil then recat = ".*" end

local num=0
for cat, sub in help.imatch( resub, recat ) do
    num = num +1;
    if silent ~= 1 then
        print(num, cat, sub);
    end
end

