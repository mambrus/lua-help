#!/usr/bin/awk -f
#
# Split manual text-page C-API into file-name pieces, one file for each
# topic (i.e. function)
#
# This script is tuned and fitting for "capi" and "caux"
#

/^([[:alpha:]]+ )+\*?luaL?_[[:alpha:]]+ \(/ {
    n = match($0, /luaL?_[[:alpha:]]+/, a)
    curr_filename = a[0] ".txt"
    if (verbose != "no") {
        print "Processing: " curr_filename
    }
}
{
    print > curr_filename
}
