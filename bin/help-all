#!/bin/bash

# *****************************************************************************
#   Generates help-file snippets for all Lua API:s in VM
#   Copyright: Michael Ambrus, 2023
#
#   For each API/variable, create a file API.txt in current directory under
#   each category "funcs/" or "vars/". F.ex:
#
#   * funcs/os.rename.txt
#   * vars/package.cpath.txt
# *****************************************************************************

WORK_DIR=$1
LUA_VERSION=$2
GEN_VERBOSE=$3
BASE=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")/.." &> /dev/null && pwd)
export PATH=$BASE/bin:$PATH

function extract_functions() {
    [ -d funcs ] || mkdir -p funcs
    cat manual.asc |
        sed -nEe '/^  [[:alpha:]]+([.:][[:alpha:]]+)? \(/,/(__| –)/P' \
            > .tmp/funcs.txt

    cat .tmp/funcs.txt |
        grep -Ev \
            -e '^[^ ]' \
            -e '^  [[:digit:]]\.[[:digit:]]\.[[:digit:]]' \
            -e '_{3,}' -e '[[:digit:]] –' \
        > .tmp/funcs1.txt
    (
        cd funcs
        cat ../.tmp/funcs1.txt | split-manual.awk -v verbose=$GEN_VERBOSE
    )
}

function extract_variables() {
    [ -d vars ]  || mkdir -p vars
    cat manual.asc |
        sed -nEe '/^  _?[[:alpha:]]+([.:][[:alpha:]]+)?$/,/(__| –)/P' \
            > .tmp/vars.txt

    cat .tmp/vars.txt |
        grep -Ev \
            -e '^[^ ]' \
            -e '^  [[:digit:]]\.[[:digit:]]\.[[:digit:]]' \
            -e '_{3,}' -e '[[:digit:]] –' \
        > .tmp/vars1.txt
    (
        cd vars
        cat ../.tmp/vars1.txt | split-manual.awk -v verbose=$GEN_VERBOSE
    )
}

function extract_capi() {
    [ -d capi ]  || mkdir -p capi
    cat manual.asc |
        sed -nEe '/^([[:alpha:]]+ )+\*?lua_[[:alpha:]]+ \(/,/(__| –)/P' \
            > .tmp/capi.txt

    cat .tmp/capi.txt |
        grep -Ev \
            -e '^  [[:digit:]]\.[[:digit:]]\.[[:digit:]]' \
            -e '_{3,}' -e '[[:digit:]] –' \
        > .tmp/capi1.txt
    (
        cd capi
        cat ../.tmp/capi1.txt | split-manual-capi.awk -v verbose=$GEN_VERBOSE
    )
}

function extract_caux() {
    [ -d caux ]  || mkdir -p caux
    cat manual.asc |
        sed -nEe '/^([[:alpha:]]+ )+\*?luaL_[[:alpha:]]+ \(/,/(__| –)/P' \
            > .tmp/caux.txt

    cat .tmp/caux.txt |
        grep -Ev \
            -e '^  [[:digit:]]\.[[:digit:]]\.[[:digit:]]' \
            -e '_{3,}' -e '[[:digit:]] –' \
        > .tmp/caux1.txt
    (
        cd caux
        cat ../.tmp/caux1.txt | split-manual-capi.awk -v verbose=$GEN_VERBOSE
    )
}


set -e
[ -d $WORK_DIR ] || mkdir -p $WORK_DIR

(
    cd $WORK_DIR
    [ -f lock_zip-all.pid ] && {
        echo ""
        echo -e "zip-all FATAL ERROR: Race detected:\n"\
            "   Competing PID: $(cat lock_zip-all.pid)\n" \
            "   Our PID:       $$" >&2
            return 1
    }
    echo $$ > lock_zip-all.pid

    [ -d .tmp ]  || mkdir -p .tmp
    if [ ! -f manual.asc ]; then
        lynx -dump https://www.lua.org/manual/${LUA_VERSION}/manual.html > \
            manual.asc
    fi
    extract_functions
    extract_variables
    extract_capi
    extract_caux

    rm lock_zip-all.pid
)
