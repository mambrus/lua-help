#!/usr/bin/awk -f
#
# Split manual text-page into file-name pieces, one file for each topic
# (i.e. function)
#
# This script is tuned and fitting for Lua interactive "func" & "var"
#

/^  _?[[:alpha:]]/ {
    curr_filename = $1 ".txt"
    if (verbose != "no") {
        print "Processing: " curr_filename
    }
}
{
    print > curr_filename
}
