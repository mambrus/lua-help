#ifndef lua_help_local_h
#define lua_help_local_h

#include "config.h"
#include <lua.h>
#include <zip.h>

#define UNUSED(X)  ((void)(X))
#define BUF_SIZE 256            /* Typical size of buffers */
#define SNAME_MAX 25            /* Maximum length of a symbol-name */
#define NDIGITS_USNIGNED 10     /* Number of digits representing unsigned */

extern zip_source_t *source;
extern zip_t *archive;

extern unsigned char *help_zipdata; /* Reference to generated RO C-array,
                                       compressed archive */
unsigned char *get_help_zipdata();  /* Get reference to RO-data */
extern unsigned int help_zipdata_len;   /* Compressed size (array size) */
extern unsigned int help_ucdata_len;    /* Uncompressed total data size */
extern unsigned int help_ucfile_len;    /* Ditto but including index-sections */

/* Denotes a position of name in archive-file and it's size */
struct znode {
    const char *category;
    const char *name;
    unsigned offset;
    size_t sz;
};

/* Line of an index */
struct lidx {
    unsigned sz;
    char name[SNAME_MAX];
};

/* What type of section i.e. what it contains */
enum content {
    HEADR,                      /* Pseudo section for the whole file */
    INDEX,                      /* The section is an index */
    DATA                        /* Section is bulk-text or data */
};

/* Linked list with section as payload. Each section can be considered a
 * directory */
struct section;
struct sections {
    struct sections *prev;
    struct section *section;
    struct sections *next;
};

/* A section is a representation of the low-level compressed data
 *
 * I.e. a section can be considered like file-system i-node. I.e. it's main
 * job is to act as pointer to
 * where the data is for each particular name */
struct section {
    enum content content;       /* Type of section */
    char *name;                 /* Name of section */
    unsigned soas;              /* Start of adjacent section (or data). I.e.
                                   last byte belonging to this section + 1 */
    unsigned sz;                /* Size of section in bytes */
    unsigned entries;           /* Number of entries i.e of whatever is
                                   represented */
    unsigned data_sz;           /* Size in bytes of referring text */
    unsigned sot;               /* Start of text */
};
extern struct sections *sections;   /* Head of linked list */

/* Inner API */
int read_lidx(zip_file_t * zfp, struct lidx *lidx);
zip_file_t *zip_fopen_offset(zip_t * archive,
                             const char *name,
                             unsigned offset, zip_flags_t flags);
/* Iterators */
int l_cat(lua_State * L);
int l_sub(lua_State * L);
int l_rematch(lua_State * L);

/* Module API */
int init_zipper(void);
int zip_locate(const char *category, const char *name, struct znode *znode);

#endif                          //lua_help_local_h
