#!/bin/bash

# *****************************************************************************
#   Run CI tests
#   Copyright: Michael Ambrus, 2023
# *****************************************************************************

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

eval V=($(cat "${SCRIPT_DIR}/../VERSIONS.TXT"))
echo -n "Test versions: "
declare -p V
declare -A R
for I in $(seq 0 ${#V[@]}); do
    [ -z "${V[$I]}" ] || {
        echo "Testing: ${V[$I]}"
        $SCRIPT_DIR/base-test.exp "${V[$I]}"
        if [ $? -eq 0 ]; then
            R[${V[$I]}]="OK"
        else
            R[${V[$I]}]="BAD"
        fi
    }
done
echo "Results:"
for K in "${!R[@]}"; do
  printf "%-20s : %s\n" $K ${R[$K]}
done

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
