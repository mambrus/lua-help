include(CheckIncludeFiles)
include(CheckLibraryExists)

## If built by luarocks the following will be set:
## - LUA
## - LUA_INCLUDE_DIR
## - LUA_LIBRARY_DIR

find_program(EXE_BREW brew)
if(NOT "${EXE_BREW}" STREQUAL "EXE_BREW-NOTFOUND")
    get_filename_component(BREW_BASE ${EXE_BREW} DIRECTORY)
    get_filename_component(BREW_BASE ${BREW_BASE} DIRECTORY)
    set(BREW_INCLUDE "${BREW_BASE}/include")
    message(STATUS "brew found: ${EXE_BREW}")
    message(STATUS "Base include & lib: ${BREW_INCLUDE}")
endif()
mark_as_advanced(EXE_BREW)

if (LUA)
    message(STATUS "Using luarocks LUA: ${LUA}")
    message(STATUS "   LUA_INCLUDE_DIR: ${LUA_INCLUDE_DIR}")
    message(STATUS "   LUA_LIBRARY_DIR: ${LUA_LIBRARY_DIR}")
    set(EXE_PLUA ${LUA})
else ()
    find_program(EXE_PLUA lua)
endif ()

if(NOT "${EXE_PLUA}" STREQUAL "EXE_PLUA-NOTFOUND")
    # Ask this lua what version it is
    execute_process(
        COMMAND ${EXE_PLUA} -e "V=string.gsub(_VERSION,\"Lua \",\"\");print(V)"
        OUTPUT_VARIABLE SYSTEM_LUA_VERSION
    )
    string(STRIP ${SYSTEM_LUA_VERSION} SYSTEM_LUA_VERSION)
    message(STATUS "Lua found in PATH: ${EXE_PLUA} version: ${SYSTEM_LUA_VERSION}")
endif ()
mark_as_advanced(EXE_PLUA)

if (SYSTEM_LUA_VERSION)
    set(LUA_VERSION_DEFAULT "${SYSTEM_LUA_VERSION}")
else ()
    set(LUA_VERSION_DEFAULT "5.3")
endif ()

set(LUA_VERSION
    ${LUA_VERSION_DEFAULT}
    CACHE STRING
    "Lua-version to support")
message(STATUS "LUA_VERSION: ${LUA_VERSION}")

find_program(EXE_LUA lua${LUA_VERSION})
if(NOT "${EXE_LUA}" STREQUAL "EXE_LUA-NOTFOUND")
    message(STATUS "Version specific Lua: ${EXE_LUA}")
    get_filename_component(LUA_BASE "${EXE_LUA}" DIRECTORY)
    get_filename_component(LUA_BASE "${LUA_BASE}" DIRECTORY)
    message(STATUS "@ base: ${LUA_BASE}")
endif ()
mark_as_advanced(EXE_LUA)

string(
    REPLACE
    "."
    ""
    _LUA_VERSION_NDOT
    ${LUA_VERSION})

set(LUA_VERSION_NDOT
    ${_LUA_VERSION_NDOT}
    CACHE STRING
    "This should not be changed")
mark_as_advanced(LUA_VERSION_NDOT)

if (LUA_INCLUDE_DIR)
    message(STATUS "Build aginst Lua-headers in: ${LUA_INCLUDE_DIR}")
else ()
    message(STATUS "LUA_INC_DIR not set. Searching for ${LUA_VERSION}")
    find_path(LUA_INCLUDE_DIR
        NAMES
            lua.h
        PATHS
            ${BREW_INCLUDE}/lua-${LUA_VERSION}
            ${BREW_INCLUDE}/lua${LUA_VERSION}
            ${LUA_BASE}/include/lua${LUA_VERSION}
            /usr/local/include/lua${LUA_VERSION}
            /usr/include/lua${LUA_VERSION}
    )
endif ()
if(LUA_INCLUDE_DIR STREQUAL "LUA_INCLUDE_DIR-NOTFOUND")
    message(FATAL_ERROR "${LUA_INCLUDE_DIR}")
else ()
    message(STATUS "LUA_INCLUDE_DIR: ${LUA_INCLUDE_DIR}")
endif ()
include_directories("${LUA_INCLUDE_DIR}")

if (LUA_LIBRARY_DIR)
    message(STATUS
        "LUA_LIBRARY_DIR is preset to ${LUA_LIBRARY_DIR}. Don't bother with linkage"
    )
else ()
    message(STATUS "LUA_LIBRARY_DIR not set. Searching for ${LUA_VERSION}")
    # Note: If library is system installed:
    #  * It doesn't need to be included in PATHS
    #  * It will be found first if others exist
    find_library(LIBLUA
        NAMES
            lua${LUA_VERSION}
            lua
        PATHS
            ${BREW_BASE}/opt/lua@${LUA_VERSION}/lib
            ${BREW_BASE}/lib
            /usr/local/lib/lua/${LUA_VERSION}
    )

    if (LIBLUA STREQUAL "LIBLUA-NOTFOUND")
        message(FATAL_ERROR
            "liblua not found. Please install version lib${LUA_VERSION}-dev")
    else ()
        message(STATUS "Found liblua version ${LUA_VERSION}: ${LIBLUA}")
    endif ()
    get_filename_component(LUA_LIBRARY_DIR ${LIBLUA} DIRECTORY)
    get_filename_component(LUA_LIBRARY_DIR_FILE ${LIBLUA} NAME)
    message(STATUS "liblua name: ${LUA_LIBRARY_DIR_FILE}")
    message(STATUS "liblua path: ${LUA_LIBRARY_DIR}")

    # Integers emerged in Lua 5.0
    check_library_exists(${LIBLUA} lua_pushinteger "" HAVE_LIB_LUA)
    if (HAVE_LIB_LUA)
        set(LUA_HELP_LIBS ${LUA_HELP_LIBS} lua${LUA_VERSION})
        add_compile_options(-L ${LUA_LIBRARY_DIR})
    else ()
        message(WARNING "Lua-library test fail for ${LIBLUA}")
    endif ()
endif ()
