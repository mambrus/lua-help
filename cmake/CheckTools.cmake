#
# CLI-tools necessary for build
#

# System-tools part of standard POSIX distributions but possibly not ultra
# thin containers (Alps etc) or obscure OS:es (Windows, OSX etc)
#
# These are not expected to fail
#
find_program(EXE_XXD
    xxd
)
mark_as_advanced(EXE_XXD)
if(EXE_XXD STREQUAL "EXE_XXD-NOTFOUND")
    message(FATAL_ERROR "Please install xxd")
endif()

find_program(EXE_AWK
    awk
)
mark_as_advanced(EXE_AWK)
if(EXE_AWK STREQUAL "EXE_AWK-NOTFOUND")
    message(FATAL_ERROR "Please install awk")
endif()

find_program(EXE_SED
    sed
)
mark_as_advanced(EXE_SED)
if(EXE_SED STREQUAL "EXE_SED-NOTFOUND")
    message(FATAL_ERROR "Please install sed")
endif()

find_program(EXE_BASH
    bash
)
mark_as_advanced(EXE_BASH)
if(EXE_BASH STREQUAL "EXE_BASH-NOTFOUND")
    message(FATAL_ERROR "Please install bash")
endif()


# System-tools not usually part of standard POSIX distributions
# These are expected to fail
#
find_program(EXE_XXD
    xxd
)
mark_as_advanced(EXE_XXD)
if(EXE_XXD STREQUAL "EXE_XXD-NOTFOUND")
    message(FATAL_ERROR "Please install xxd")
endif()

find_program(EXE_LYNX
    lynx
)
mark_as_advanced(EXE_LYNX)
if(EXE_LYNX STREQUAL "EXE_LYNX-NOTFOUND")
    message(FATAL_ERROR "Please install lynx")
endif()

find_program(EXE_ZIPTOOL
    ziptool
)
mark_as_advanced(EXE_ZIPTOOL)
if(EXE_ZIPTOOL STREQUAL "EXE_ZIPTOOL-NOTFOUND")
    message(FATAL_ERROR "Please install ziptool")
endif()

find_program(EXE_TEE
    tee
)
mark_as_advanced(EXE_TEE)
if(EXE_TEE STREQUAL "EXE_TEE-NOTFOUND")
    message(FATAL_ERROR "Please install tee")
endif()

# Local tools. Not only sanity-test, get their full-paths
#
find_program(EXE_SPLIT_MANUAL
    split-manual.awk
    PATHS ${CMAKE_CURRENT_SOURCE_DIR}/bin
)
mark_as_advanced(EXE_SPLIT_MANUAL)
if(EXE_SPLIT_MANUAL STREQUAL "EXE_SPLIT_MANUAL-NOTFOUND")
    message(FATAL_ERROR "bin/split-manual.awk missing from project")
endif()

find_program(EXE_HELP_ALL
    help-all
    PATHS ${CMAKE_CURRENT_SOURCE_DIR}/bin
)
mark_as_advanced(EXE_HELP_ALL)
if(EXE_HELP_ALL STREQUAL "EXE_HELP_ALL-NOTFOUND")
    message(FATAL_ERROR "bin/help-all missing from project")
endif()

find_program(EXE_ZIP_ALL
    zip-all
    PATHS ${CMAKE_CURRENT_SOURCE_DIR}/bin
)
mark_as_advanced(EXE_ZIP_ALL)
if(EXE_ZIP_ALL STREQUAL "EXE_ZIP_ALL-NOTFOUND")
    message(FATAL_ERROR "bin/zip-all missing from project")
endif()

find_program(EXE_ZIP2C
    zip2c
    PATHS ${CMAKE_CURRENT_SOURCE_DIR}/bin
)
mark_as_advanced(EXE_ZIP2C)
if(EXE_ZIP2C STREQUAL "EXE_ZIP2C-NOTFOUND")
    message(FATAL_ERROR "bin/zip2c missing from project")
endif()
