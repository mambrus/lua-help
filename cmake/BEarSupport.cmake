option(XB_LUA_EXPORT_COMPILE_COMMANDS
    "Generate compile_commands.json" ON)
mark_as_advanced(XB_LUA_EXPORT_COMPILE_COMMANDS)

set (CMAKE_EXPORT_COMPILE_COMMANDS
    ${XB_LUA_EXPORT_COMPILE_COMMANDS}
    CACHE BOOL
    "Overloaded cmake global cached CMAKE_EXPORT_COMPILE_COMMANDS"
    FORCE
)
