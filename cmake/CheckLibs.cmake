include(CheckLibraryExists)
include(CheckTypeSize)
include(CheckCSourceCompiles)
include(CheckIncludeFiles)
include(CheckFunctionExists)
include(CheckSymbolExists)

CHECK_LIBRARY_EXISTS(zip zip_source_buffer_create  "" HAVE_LIB_ZIP)

if (HAVE_LIB_ZIP)
    set(LUA_HELP_LIBS ${LUA_HELP_LIBS} zip)
else ()
    message(FATAL_ERROR "Please install libzip-dev")
endif ()
